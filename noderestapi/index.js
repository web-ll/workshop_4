// Import express
let express = require('./node_modules/express');
// Import Body parser
let bodyParser = require('./node_modules/body-parser');
// Import Mongoose
let mongoose = require('mongoose');
// Initialise the app
let app = express();

const {
    base64decode
  } = require('nodejs-base64');
const jwt = require("jsonwebtoken");
const theSecretKey = '123';

// Import routes
let apiRoutes = require("./api-routes");
// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/noderestapi', { useNewUrlParser: true});
var db = mongoose.connection;

// Added check for DB connection
if(!db)
    console.log("Error connecting db")
else
    console.log("Db connected successfully")

// Setup server port
var port = process.env.PORT || 8080;
  

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express'));

// JWT Authentication
app.use(function (req, res, next) {
  if (req.headers["authorization"]) {
    const authToken = req.headers['authorization'].split(' ')[1];
    try {
      jwt.verify(authToken, theSecretKey, (err, decodedToken) => {
        if (err || !decodedToken) {
          res.status(401);
          res.json({
            error: "Unauthorized "
          });
        }
        console.log('Welcome', decodedToken.name);
        next();
        // if (decodedToken.userId == 123) {
        //   next();
        // }
      });
    } catch (e) {
      next();
    }

  } else {
    res.status(401);
    res.send({
      error: "Unauthorized "
    });
  }
});
// Use Api routes in the App
app.use('/api', apiRoutes);
// Launch app to listen to specified port

app.listen(port, function () {
    console.log("Running RestHub on port " + port);
});